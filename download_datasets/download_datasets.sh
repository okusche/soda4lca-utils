#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset


usage()
{
    echo
    echo "    usage: download_datasets.sh [-b <node base URL>] [-a] -i <UUID file>"
    echo
}


do_dl()
{
	if [ ! -f "${__input}" ]; then
    	echo "input file ${__input} does not exist."
    	exit
	fi
	
    totallines=`wc -l < "${__input}"`
    totalcounter=1
	counter=1

	if [ -d "dl_tmp" ]; then
		rm -Rf dl_tmp
	fi
	mkdir -p dl_tmp

	if [ $__append -ne 1 ] && [ -d "dl_datasets" ]; then
		rm -Rf dl_datasets
	fi
	mkdir -p dl_datasets
	

	while IFS="" read -r p || [ -n "$p" ]
	do
  	    echo downloading dataset $totalcounter / $totallines   $p
	   
		curl -S -s --output-dir dl_tmp -OJ $__nodeurl/resource/processes/$p/zipexport
		
		filename="${p}_dependencies.zip"
		unzip -qq -n "dl_tmp/$filename" -d dl_datasets
		((++totalcounter))
	done < ${__input}

}



__append=0
__nodeurl="https://www.oekobaudat.de/OEKOBAU.DAT"
__input=

while getopts "ab:i:" o; do
    case "${o}" in
        a)
            __append=1
            ;;
        i)
            __input=${OPTARG}
            ;;
        b)
            __nodeurl=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${__input}" ]; then
    usage
fi

do_dl