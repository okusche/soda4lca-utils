#!/bin/zsh

# define variables

MYSQL_BIN_DIR=/Applications/MAMP/Library/bin

# the current_versionectory with the unzipped soda4LCA binaries, we expect the folders to be named "3.0.0" etc.
SODA_VERSIONS_DIR=/Users/oli/scratch/soda4LCA_versions/WARs

DB_DUMPS_DIR=/Users/oli/scratch/soda4LCA_versions/db_dumps
CATALINA_HOME_DIR=/Applications/Entwicklung/apache-tomcat-8.5.73
CATALINA_BASE_DIR=/Applications/Entwicklung/apache-tomcat-instance-migrate
ORIGINAL_DB_DUMP=/Users/oli/Nextcloud/Uploads/JRC/lcdn22023_mod.sql
DB_SCHEMA_NAME=lcdn_2023
DB_USER=root
DB_PASS=root

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

usage()
{
	echo
    echo "    usage: migrate_db.sh [-f <from-version>] [-d <file to delete>] [-r]"
    echo
    echo "      -f <from-version>    Start migrating starting with specified version. The DB schema"
    echo "                           will be restored to 'pre-<from-version>.sql'."
    echo
    echo "      -d <file to delete>  Deletes the specified file from all binary WARs. Full path needs to be specified,"
    echo "                           e.g. WEB-INF/classes/de/iai/ilcd/db/migrations/V7_1_1__ExtractProcessDataSources.class"
    echo
    echo "      -r                   Restores the DB schema to the original one."
    echo
}


# dump_db_pre <version>
dump_db_pre () {
    readonly version=${1:?"The version must be specified."}
    dump_db pre "$version"
}

# dump_db_post <version>
dump_db_post () {
    readonly version=${1:?"The version must be specified."}
    dump_db post "$version"
}

# dump_db <prefix> <version>
dump_db () {
    readonly prefix=${1:?"The prefix must be specified."}
    readonly version=${2:?"The version must be specified."}
    $MYSQL_BIN_DIR/mysqldump -u $DB_USER -p$DB_PASS --databases lcdn_2023 > "${DB_DUMPS_DIR}/${DB_SCHEMA_NAME}_${prefix}_${version}.sql"
}

#restore_db_from <from-version>
restore_db_from () {
    readonly from=${1:?"The from version must be specified."}
    $MYSQL_BIN_DIR/mysql -s -N -u $DB_USER -p$DB_PASS -e "DROP SCHEMA IF EXISTS \`${DB_SCHEMA_NAME}\`; CREATE SCHEMA \`${DB_SCHEMA_NAME}\` DEFAULT CHARACTER SET utf8;"
    $MYSQL_BIN_DIR/mysql -u $DB_USER -p$DB_PASS $DB_SCHEMA_NAME < "${DB_DUMPS_DIR}/${DB_SCHEMA_NAME}_pre_${from}.sql"

}

restore_original_db () {
    $MYSQL_BIN_DIR/mysql -s -N -u $DB_USER -p$DB_PASS -e "DROP SCHEMA IF EXISTS \`${DB_SCHEMA_NAME}\`; CREATE SCHEMA \`${DB_SCHEMA_NAME}\` DEFAULT CHARACTER SET utf8;"
    $MYSQL_BIN_DIR/mysql -u $DB_USER -p$DB_PASS $DB_SCHEMA_NAME < "${ORIGINAL_DB_DUMP}"
}

start_tomcat () {
    export CATALINA_HOME=$CATALINA_HOME_DIR
    export CATALINA_BASE=$CATALINA_BASE_DIR
    rm $CATALINA_BASE_DIR/logs/catalina.out
    tail -n0 -F $CATALINA_BASE_DIR/logs/catalina.out &
    me=$!
    trap "kill $me" INT TERM HUP QUIT EXIT
    $CATALINA_HOME_DIR/bin/startup.sh
    pause ""
    $CATALINA_HOME_DIR/bin/shutdown.sh
}

pause()
{
    echo "$*"; read -k1 -s
}


#delete_file <file name>
delete_file () {
    readonly file=${1:?"The file name must be specified."}
    echo "removing ${file} from ZIPs..."
    find ${SODA_VERSIONS_DIR} -name Node.war -mindepth 1 -maxdepth 5 -type f -exec zip -q -d {} "${file}" \;
}



__from_version=
__filename=


while getopts "f:d:r" o; do
    case "${o}" in
        f)
            __from_version=${OPTARG}
            restore_db_from "${__from_version}"
            ;;
        r)
            echo "restoring original DB"
            restore_original_db
            exit
            ;;
        d)
            __filename=${OPTARG}
            echo "deleting file ${__filename} from binaries..."
            delete_file ${__filename}
            exit
            ;;
        *)
            usage
            exit
            ;;
    esac
done
shift $((OPTIND-1))



# read all current_versionectories in $SODA_VERSIONS_DIR into an array (uses BSD variant of find)
soda_versions=("${(@f)$(find ${SODA_VERSIONS_DIR} -mindepth 1 -maxdepth 1 -type d -exec basename {} \; | sort)}")

i=1

for current_version in "${soda_versions[@]}"; do
    echo "$((i++)) $current_version"
    if [[ "$current_version" < "${__from_version}" ]]; then
      # if from version is specified, skip anything below it
      continue
    fi
    # now execute
    #echo "$current_version is greater than ${__from_version}"
    echo "migrating ${current_version}"
    dump_db_pre "${current_version}"
    restore_original_db
    cp "${SODA_VERSIONS_DIR}/${current_version}/bin/Node.war" "${CATALINA_BASE_DIR}/webapps/"
    start_tomcat
    dump_db_post "${current_version}"

done


