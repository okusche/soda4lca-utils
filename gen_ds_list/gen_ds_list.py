"""
This script reads an ILCD ZIP file and generates an XLSX document with the UUIDs of all the process datasets in the ZIP
in the "dataset type"/"UUID"/"version" format that can be used for batch assignments in soda4LCA.
"""

import os
import sys
from os.path import exists
from zipfile import ZipFile

import pandas as pd
import xlsxwriter

COLUMN_VERSION = 'version'

COLUMN_UUID = 'uuid'

COLUMN_PATH = 'path'

COLUMN_DATASET_TYPE = 'dataset type'

PATH_PREFIX_ILCD = 'ILCD/'
PATH_PREFIX_LCMODELS = PATH_PREFIX_ILCD + 'lifecyclemodels/'
PATH_PREFIX_PROCESSES = PATH_PREFIX_ILCD + 'processes/'
PATH_PREFIX_FLOWS = PATH_PREFIX_ILCD + 'flows/'
PATH_PREFIX_FLOWPROPERTIES = PATH_PREFIX_ILCD + 'flowproperties/'
PATH_PREFIX_UNITGROUPS = PATH_PREFIX_ILCD + 'unitgroups/'
PATH_PREFIX_SOURCES = PATH_PREFIX_ILCD + 'sources/'
PATH_PREFIX_CONTACTS = PATH_PREFIX_ILCD + 'contacts/'
PATH_PREFIX_LCIAMETHODS = PATH_PREFIX_ILCD + 'lciamethods/'

DS_TYPE_LCMODEL = 'life cycle model data set'
DS_TYPE_PROCESS = 'process data set'
DS_TYPE_FLOW = 'flow data set'
DS_TYPE_FLOWPROPERTY = 'flow property data set'
DS_TYPE_UNITGROUP = 'unit group data set'
DS_TYPE_SOURCE = 'source data set'
DS_TYPE_CONTACT = 'contact data set'
DS_TYPE_LCIAMETHOD = 'LCIA method data set'

ALLOWED_PATH_PREFIXES = [PATH_PREFIX_LCMODELS, PATH_PREFIX_PROCESSES, PATH_PREFIX_FLOWS, PATH_PREFIX_FLOWPROPERTIES,
                         PATH_PREFIX_UNITGROUPS, PATH_PREFIX_SOURCES, PATH_PREFIX_CONTACTS, PATH_PREFIX_LCIAMETHODS]
DATASET_TYPES = [DS_TYPE_LCMODEL, DS_TYPE_PROCESS, DS_TYPE_FLOW, DS_TYPE_FLOWPROPERTY, DS_TYPE_UNITGROUP,
                 DS_TYPE_SOURCE, DS_TYPE_CONTACT, DS_TYPE_LCIAMETHOD]


def get_ds_type(path):
    """
    determines the dataset type for a path based on the path prefix ('ILCD/flows/' etc.)

    :param path: the full path of the file inside the ZIP
    :return: the dataset type as per the ILCD spec (e.g. 'flow data set')
    """
    for index, item in enumerate(ALLOWED_PATH_PREFIXES):
        if path.startswith(item):
            return DATASET_TYPES[index]
    return 'unknown'


def extract_uuid(path):
    """
    extracts the uuid from a path like 'ILCD/flows/0a2a71f2-4f59-4a67-b5f1-d633039ce0ea_00.00.000.xml'

    :param path: the full path of the file inside the ZIP
    :return: only the UUID from the file name in lower case
    """
    for index, item in enumerate(ALLOWED_PATH_PREFIXES):
        if path.startswith(item):
            return path[len(item): len(item) + 36].lower()
    return 'unknown'


# specifying the zip file name
file_name = ""

try:
    file_name = sys.argv[1]
except IndexError:
    raise SystemExit(f"Usage: {sys.argv[0]} <ZIP to process>")

if not(exists(file_name)):
    raise SystemExit(f"File {file_name} does not exist")
print("Processing " + file_name)


file_name_base_name = os.path.basename(file_name)
result_file_name = file_name_base_name + '_processes' + '.xlsx'

# open the zip file in READ mode and read all entries
with ZipFile(file_name, 'r') as zip:
    # print all the contents of the zip file
    # zip.printdir()

    entries = zip.namelist()
    # print(entries)

df = pd.DataFrame(entries, columns=[COLUMN_PATH])

# print(df)

# throw out non-dataset entries
df = df[df[COLUMN_PATH].apply(lambda x: any(x.startswith(item) for item in ALLOWED_PATH_PREFIXES))]

# throw out directory entries
df = df[df[COLUMN_PATH].apply(lambda x: not (x.endswith("/")))]

# add dataset type
df[COLUMN_DATASET_TYPE] = df[COLUMN_PATH].apply(lambda x: get_ds_type(x))

# extract UUID
df[COLUMN_UUID] = df[COLUMN_PATH].apply(lambda x: extract_uuid(x))

# add (empty) version column
df[COLUMN_VERSION] = ""

# reduce to process datasets only
df = df[df[COLUMN_DATASET_TYPE].apply(lambda x: x == DS_TYPE_PROCESS)]

# prepare for xlsx export
df = df[[COLUMN_DATASET_TYPE, COLUMN_UUID, COLUMN_VERSION]]

# pd.set_option('display.max_columns', 3)
# print(df)

# write to xlsx
writer = pd.ExcelWriter(result_file_name)
df.to_excel(writer, sheet_name=file_name_base_name, index=False, engine=xlsxwriter)

# Auto-adjust columns' width
for column in df:
    column_width = max(df[column].astype(str).map(len).max(), len(column))
    col_idx = df.columns.get_loc(column)
    writer.sheets[file_name_base_name].set_column(col_idx, col_idx, column_width)

writer.close()
